#!/bin/bash

pip install -r requirements.txt -c constraints.txt -t lib

cat << EOF > app.yaml
runtime: python37

handlers:
  - url: /.*
    secure: always
    redirect_http_response_code: 301
    script: auto

env_variables:
  CLOUD_SQL_USERNAME: '${CLOUD_SQL_USERNAME}'
  CLOUD_SQL_PASSWORD: '${CLOUD_SQL_PASSWORD}'
  CLOUD_SQL_DATABASE_NAME: '${CLOUD_SQL_DATABASE_NAME}'
  CLOUD_SQL_CONNECTION_NAME: '${CLOUD_SQL_CONNECTION_NAME}'
EOF

gcloud app deploy --quiet
