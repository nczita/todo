# TODO

Simple TODO application, should allow:

- adding new todo action (title, description, due date)
- editing todo action
- deleting todo action
- show list of todo actions


## Migrations

```
dco up --build -d
dco run --rm todo python main.py db upgrade
```

If you remove migrations folder:
```
dco up --build -d
dco run --rm todo python main.py db init
dco run --rm todo python main.py db migrate
```

Also when new models are add/removed/changed:
```
dco up --build -d
dco run --rm todo python main.py db migrate
```

## Deploy to AppEngine

### Install Google Cloud SDK

- https://cloud.google.com/sdk/docs/

```bash
curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-224.0.0-darwin-x86_64.tar.gz -o google-cloud-sdk-224.0.0-darwin-x86_64.tar.gz
tar xzf google-cloud-sdk-224.0.0-darwin-x86_64.tar.gz
cd google-cloud-sdk
./install.sh
```

### Create new GCP project - we could have one AppEngine per project

- https://cloud.google.com/resource-manager/docs/creating-managing-projects
- https://cloud.google.com/appengine/docs/admin-api/creating-an-application

gcloud config set project gildia-222415

### Create App Engine in previously created project, please select region that is cheap and in Europe

- https://cloud.google.com/appengine/docs/standard/python3/quickstart
- https://cloud.google.com/sql/pricing

### Now you could try to deploy our App

- https://cloud.google.com/appengine/docs/standard/python/tools/using-libraries-python-27
- https://cloud.google.com/appengine/docs/standard/python3/testing-and-deploying-your-app
- https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard_python37/hello_world
- https://cloud.google.com/appengine/docs/standard/python3/config/appref

Required libraries:
```bash
pip install -r requirements.txt -c constraints.txt -t lib
```

Deploy:
```bash
gcloud app deploy
```

If `gcloud` doesn't have selected project, follow steps printed to select project.

### Try to open our application

```bash
gcloud app browse
```

Should show only _Internal error_, as we need to provide DB for it.

### Create DB

- https://cloud.google.com/sql/docs/mysql/create-instance
- https://cloud.google.com/sql/docs/mysql/create-manage-users
- https://cloud.google.com/sql/docs/mysql/create-manage-databases

Go to your GCP project web console and select SQL.
Click create instance, select:
- mysql
- mysql second generation
- provide instance ID and root password
- chose region (same as AppEngine) and any Zone

After DB instance is started, create user and database.
At tab users click _Create user account_ provide user name and password.
At tab databases click _Create database_ provide database name and other options.

### Install and set Cloud SQL Proxy

- https://cloud.google.com/sql/docs/mysql/sql-proxy
- https://cloud.google.com/sql/docs/mysql/connect-admin-proxy

Install:
```bash
curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.amd64
chmod +x cloud_sql_proxy
```

Set up:
```bash
./cloud_sql_proxy -instances=[PROJECT_NAME]:[REGION]:[DATABASE_INSTANCE_NAME]=tcp:3306
# for example
./cloud_sql_proxy -instances=gildia-222415:europe-west2:gildia=tcp:3306
```

Validate:
```bash
mysql -u [DB_USERNAME] -p --host 127.0.0.1
# for example
mysql -u gildia -p --host 127.0.0.1
```

### Migrate DB

Migration could be done by application, just create proper `DATABASE_URL`:
```bash
export DATABASE_URL=mysql://[DB_USERNAME]:[DB_PASSWORD]@[PROXY_ADDRESS]:[PROXY_PASSWORD]/[DB_NAME]
# example
export DATABASE_URL=mysql://todo_user:todo_pass@127.0.0.1:3306/todo

python main.py db upgrade
# or use docker (PROXY_ADDRESS cannot be 127.0.0.1/localhost in this case)
docker-compose run --rm -e DATABASE_URL=${DATABASE_URL} todo python main.py db upgrade
```

### Set database access for application

- https://cloud.google.com/appengine/docs/standard/python3/using-cloud-sql

Set environment variables for access DB:
```bash
export CLOUD_SQL_USERNAME='todo_user'
export CLOUD_SQL_PASSWORD='todo_pass'
export CLOUD_SQL_DATABASE_NAME='todo'
export CLOUD_SQL_CONNECTION_NAME='todo-123456:europe-west2:todo'
```

Set them to `app.yaml`:
```bash
cat << EOF > app.yaml
runtime: python37

handlers:
  - url: /.*
    secure: always
    redirect_http_response_code: 301
    script: auto

env_variables:
  CLOUD_SQL_USERNAME: '${CLOUD_SQL_USERNAME}'
  CLOUD_SQL_PASSWORD: '${CLOUD_SQL_PASSWORD}'
  CLOUD_SQL_DATABASE_NAME: '${CLOUD_SQL_DATABASE_NAME}'
  CLOUD_SQL_CONNECTION_NAME: '${CLOUD_SQL_CONNECTION_NAME}'
EOF
```

Deploy app with changes:
```bash
gcloud app deploy
```

Or use script:
```bash
./deploy.sh
```

### Try out our app

```bash
gcloud app browse
```

Works like a charm !!!


Cloud Build

- https://cloud.google.com/cloud-build/docs/configuring-builds/build-test-deploy-artifacts
- https://cloud.google.com/compute/docs/access/service-accounts
- https://cloud.google.com/cloud-build/docs/build-config
- https://cloud.google.com/cloud-build/docs/securing-builds/use-encrypted-secrets-credentials#example_build_request_using_an_encrypted_variable
- https://cloud.google.com/cloud-build/docs/build-config
- https://cloud.google.com/cloud-build/docs/create-custom-build-steps
- https://cloud.google.com/source-repositories/docs/quickstart-triggering-builds-with-source-repositories


- permissions - service agent, roles
- enable API
- keys KMS

## Deploy to K8S

Based on https://github.com/helm/charts/tree/master/stable/drupal . 

Install via helm:

```bash
$ helm install todo-fleet
```

Forward port to see results:

```bash
$ export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=todo-fleet,app.kubernetes.io/instance=NAME_OF_RELEASE -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward $POD_NAME 8080:8000
```

Delete release:

```bash
$ helm delete NAME_OF_RELEASE --purge
```

Package and upload chart:

```bash
$ helm package todo-fleet
```

```bash
gsutil acl ch -u AllUsers:R gs://todo-charts/todo-fleet-0.1.0.tgz
gsutil cp todo-fleet-0.1.0.tgz gs://todo-charts/
```
