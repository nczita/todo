from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class TodoNote(db.Model):
    __tablename__ = 'todos'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(320), nullable=False)

    def __repr__(self):
        return '<TodoNote %r>' % self.username