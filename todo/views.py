from flask import render_template, request, flash, redirect, url_for, Blueprint
from todo.models import TodoNote, db

bp = Blueprint('Todos', __name__, template_folder='templates')

@bp.route("/")
def index_of_todos():
    todos = TodoNote.query.all()
    return render_template('index.html', todos=todos)

@bp.route("/create_todo", methods=["POST"])
def create_todo(*args, **kwargs):
    title = request.form['title']
    description = request.form['description']
    if title.strip() != "":
        td = TodoNote(title=title, description=description)
        db.session.add(td)
        db.session.commit()
        flash("Added new todo!", 'success')
    else:
        flash("Todo need title!", 'danger')
    return redirect(url_for('Todos.index_of_todos'))

@bp.route("/todo/<_id>")
def show_todo(_id):
    td = TodoNote.query.get_or_404(_id)
    return render_template('show_todo.html', todo=td)
