import os
from flask import Flask

def create_app():
    app = Flask(__name__)
    db_user = os.environ.get('CLOUD_SQL_USERNAME')
    db_password = os.environ.get('CLOUD_SQL_PASSWORD')
    db_name = os.environ.get('CLOUD_SQL_DATABASE_NAME')
    db_connection_name = os.environ.get('CLOUD_SQL_CONNECTION_NAME')
    if os.environ.get('GAE_ENV') == 'standard':
        unix_socket = '/cloudsql/{}'.format(db_connection_name)
        SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@/{}?unix_socket={}'.format(db_user, db_password, db_name, unix_socket)
    else:
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite:///:memory:')
    app.config.update(
        TESTING=True,
        TEMPLATES_AUTO_RELOAD=True,
        SECRET_KEY=b'secret key',
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SQLALCHEMY_DATABASE_URI=SQLALCHEMY_DATABASE_URI
    )
    from todo.models import db
    db.init_app(app)
    from todo.views import bp
    app.register_blueprint(bp)
    return app