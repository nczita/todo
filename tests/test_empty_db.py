import os
import tempfile
import pytest

from todo.app import create_app
from todo.models import db

@pytest.fixture
def client():
    app = create_app()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['TESTING'] = True

    client = app.test_client()

    with app.app_context():
        db.create_all(app=app)

    yield client

def test_empty_db(client):
    """Start with a blank database."""

    rv = client.get('/')
    assert rv.status == '200 OK'