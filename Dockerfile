FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt requirements-docker.txt constraints.txt ./
RUN pip install --no-cache-dir -r requirements.txt -r requirements-docker.txt -c constraints.txt

COPY . .

EXPOSE 8000
CMD [ "gunicorn", "-w", "4", "-b", "0.0.0.0:8000", "main:app" ]
